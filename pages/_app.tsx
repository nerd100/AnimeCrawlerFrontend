import { CookiesProvider } from "react-cookie"
import '../node_modules/bootstrap/dist/css/bootstrap.css'
import '../styles/globals.css';
import '../styles/global-variables.scss';
import Layout from '../components/layout/layout';
import type { AppProps } from 'next/app';


function MyApp({ Component, pageProps }: AppProps) {
  return (
    <CookiesProvider>
      <Layout>
        <Component {...pageProps} />
      </Layout>
    </CookiesProvider>

  )
}

export default MyApp
