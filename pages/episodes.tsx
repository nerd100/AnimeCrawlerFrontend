import type { GetServerSideProps, NextPage } from "next";
import Link from "next/link";
import { ReactNode, useEffect } from "react";
import { ViewStatus } from "../components/enums/enums";
import { requireAuthentication } from "../components/hoc/requireAuthentication";
import CustomButton from "../components/layout/tableButton";
import { ScraperService, EpisodeType, EpisodeAndAnimeResponse, AnimeList, EpisodeList, AnimeType } from "../components/services/ScraperService";
import './episodes.module.scss';

export const getServerSideProps: GetServerSideProps = requireAuthentication(async (ctx) => {
  const { errorCode: errorCodeTables, data: tables } = await new ScraperService().getAnimes(ctx);
  const { errorCode: errorCodeElements, data: elements } = await new ScraperService().getEpisodes(ctx);
  console.log(errorCodeElements)
  if (errorCodeElements == 500) {
    return {
      redirect: {
        destination: '/',
        permanent: false,
      }
    }
  }

  return {
    props: { errorCodeElements, tables, elements }
  }
})

function creatTables(tables: AnimeList, episodes: EpisodeList): ReactNode[] {
  let result = new Array<ReactNode>();
  tables.list.forEach((anime, counter) => {
    const animeId = anime.Id;
    const episodesForTable = episodes.list.filter(e => e.AnimeId === animeId);
    const table = createTable(anime, episodesForTable, counter);
    result.push(table);
  });

  return result
}

function createTable(anime: AnimeType, episodesForTable: EpisodeType[], counter: number): ReactNode {
  const name = anime.Name;
  const tableNumber = counter + 1;

  return (
    <div key={tableNumber} className="card text-white bg-secondary mb-3">
      <h3 className="card-header">
        {`${tableNumber.toString() + '. ' + name}`}
      </h3>
      <div className="card-body">
        <div className="table-responsive">
          <table className="table table-dark">
            <thead>
              <tr>
                <th scope="col">#</th>
                <th scope="col">AnimeId</th>
                <th scope="col">Anime</th>
                <th scope="col">Season</th>
                <th scope="col">Episode</th>
                <th scope="col">Date</th>
                <th scope="col">StatusId</th>
                <th scope="col">Status</th>
              </tr>
            </thead>
            <tbody>
              {mapListElement(episodesForTable)}
            </tbody>
          </table>
        </div>
      </div>
    </div>
  )
}

function mapListElement(episodes: EpisodeType[] | undefined): ReactNode[] {
  let result = new Array<ReactNode>();
  episodes?.forEach((elem, counter) => {
    const episode = createListEntry(elem, counter);
    result.push(episode);
  }
  );
  return result;
}

function createListEntry(episode: EpisodeType, counter: number): ReactNode {
  const isViewed = episode.StatusId == ViewStatus.Viewed;
  const date = episode.Date.toString();
  const formattedDate = new Date(date);
  const rowNumber = counter + 1;

  return (
    <tr key={rowNumber}>
      <th scope="row">{rowNumber}</th>
      <td>{episode.AnimeId}</td>
      <td>{episode.Anime}</td>
      <td>{episode.Season}</td>
      <td>{episode.Episode}</td>
      <td>{formattedDate.toLocaleDateString()}</td>
      <td>{episode.StatusId}</td>
      <td>
        {isViewed ? <CustomButton id={episode.Id} statusId={ViewStatus.Open} buttonStyle={"success"}>{episode.Status}</CustomButton> : <CustomButton id={episode.Id} statusId={ViewStatus.Viewed} buttonStyle={"warning"}>{episode.Status}</CustomButton>}
      </td>
    </tr>
  );
}

const Episodes: NextPage<EpisodeAndAnimeResponse> = ({ errorCode, tables, elements }) => {
  return (
    <>
      <div className="container">
        <div className="d-flex justify-content-end mb-3">
          <Link href="/episodes/anime">
            <button className="btn btn-success">Add Anime +</button>
          </Link>
        </div>
        {creatTables(tables, elements)}
      </div>
    </>
  );
}

export default Episodes;