import type { GetServerSideProps, NextPage } from "next";
import Link from "next/link";
import { ReactNode } from "react";
import { requireAuthentication } from "../components/hoc/requireAuthentication";
import { PersonResponse, ScraperService, PersonType } from "../components/services/ScraperService";

export const getServerSideProps: GetServerSideProps = requireAuthentication(async (ctx) => {
    const { errorCode, data } = await new ScraperService().getPersons(ctx);
    return {
        props: { errorCode, data }
    }
})

async function DeletePersonWithId(personId: string) {
    console.log(personId)
    const { errorCode, data } = await new ScraperService().deletePerson(personId);
    if (errorCode == false && data !== undefined) {
      window.location.reload();
    } else {
      console.log('ErrorCode: ' + errorCode);
      console.log(data)
    }
}

function mapListElement(personList: PersonType[] | undefined): ReactNode[] {
    let result = new Array<ReactNode>();
    personList?.forEach((elem, counter) => {
        const episode = createListEntry(elem, counter);
        result.push(episode);
    }
    );
    return result
}


function createListEntry(person: PersonType, counter: number): ReactNode {


    var personBirthday = new Date(person.Geburtstag);
    var formatedDay = ("0" + personBirthday.getDate().toString()).slice(-2);
    var formatedMonth = ("0" + (personBirthday.getMonth()+1).toString()).slice(-2);
    var formatedBirthday = `${formatedDay}.${formatedMonth}.${personBirthday.getFullYear()}`

    return (
        <tr key={counter}>
            <th scope="row">{counter}</th>
            <td>
                <Link href={`persons/${person.Id}/update`}>
                    <button className={`btn btn-warning`}>Update</button>
                </Link>
            </td>
            <td>{person.Vorname}</td>
            <td>{person.Nachname}</td>
            <td>{person.Strasse + " " + person.Hausnummer}</td>
            <td>{person.Plz}</td>
            <td>{person.Ort}</td>
            <td>{formatedBirthday}</td>
            <td>
                <button className={`btn btn-danger`} onClick={async () => DeletePersonWithId(person.Id.toString())}>Delete</button>
            </td>
        </tr>
    );
}

const Person: NextPage<PersonResponse> = ({ errorCode, data }) => {

    return (
        <>
            <div className="container">
                <div className="row">
                    <div className="col-sm">
                        <div className="d-flex justify-content-end mb-3">
                            <Link href="/persons/insert">
                                <button className="btn btn-success">Add Person +</button>
                            </Link>
                        </div>
                        <div className="table-responsive">
                            <table className="table table-dark">
                                <thead>
                                    <tr>
                                        <th scope="col">#</th>
                                        <th scope="col">Update</th>
                                        <th scope="col">Vorname</th>
                                        <th scope="col">Nachname</th>
                                        <th scope="col">Straße & Hausnummer</th>
                                        <th scope="col">Plz</th>
                                        <th scope="col">Ort</th>
                                        <th scope="col">Geburtstag</th>
                                        <th scope="col">Delete</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    {mapListElement(data?.list)}
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </>
    );
}

export default Person;