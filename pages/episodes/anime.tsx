import type { GetServerSideProps, NextPage } from "next";
import { ReactNode } from "react";
import { requireAuthentication } from "../../components/hoc/requireAuthentication";
import { useForm } from "../../components/layout/forms/formSubmit";
import { AnimeResponse, ScraperService, AnimeType, animeProps } from "../../components/services/ScraperService";

export const getServerSideProps: GetServerSideProps = requireAuthentication(async (ctx) => {
  const { errorCode, data } = await new ScraperService().getAnimes(ctx);

  return {
    props: { errorCode, data }
  }
})

function mapListElement(animes: AnimeType[] | undefined): ReactNode[] {
  let result = new Array<ReactNode>();
  animes?.forEach((elem, counter) => {
    const episode = createListEntry(elem, counter);
    result.push(episode);
  }
  );
  return result
}

async function deleteAnime(id: number): Promise<any> {
  await new ScraperService().deleteAnime(id);
  window.location.reload();
}

function createListEntry(animes: AnimeType, counter: number): ReactNode {
  return (
    <tr key={counter}>
      <th scope="row">{counter}</th>
      <td>{animes.Id}</td>
      <td>{animes.Name}</td>
      <td>{animes.Expression}</td>
      <td>
        <button className="btn btn-danger" onClick={async () => { await deleteAnime(animes.Id) }}>DELETE -</button>
      </td>
    </tr>
  );
}

const Animes: NextPage<AnimeResponse> = ({ errorCode, data }) => {
  const initialState: animeProps = {
    AnimeId: 0,
    Name: "",
    Expression: ""
  };

  const { onChange, onSubmit, values } = useForm(
    loginUserCallback,
    initialState
  );

  async function loginUserCallback() {
    const animeDetails: animeProps = values as animeProps;
    const { errorCode, data } = await new ScraperService().addAnime(animeDetails);

    if (errorCode == false && data !== undefined) {
      window.location.reload();
    } else {
      console.log('ErrorCode: ' + errorCode);
      console.log(data);
    }
  }
  return (
    <>
      <div className="container">
        <div className="row">
          <div className="col-sm">
          <div className="table-responsive">
            <table className="table table-dark">
              <thead>
                <tr>
                  <th scope="col">#</th>
                  <th scope="col">AnimeId</th>
                  <th scope="col">Name</th>
                  <th scope="col">Expression</th>
                  <th scope="col">Delete</th>
                </tr>
              </thead>
              <tbody>
                {mapListElement(data?.list)}
              </tbody>
            </table>
          </div>
          </div>
          <div className="col-sm">
            <div className="animePanel">
              <form className="form" onSubmit={onSubmit}>
                <h2 className="title">Add New Anime</h2>
                <div className="input-container">
                  <input className="input"
                    name='AnimeId'
                    id='AnimeId'
                    onChange={onChange}
                    placeholder="AnimeId"
                    min="0"
                    data-bind="value:replyNumber"
                    required
                  />
                </div>
                <div className="input-container ic2">
                  <input className="input"
                    name='Name'
                    id='Name'
                    onChange={onChange}
                    placeholder="Name"
                    required
                  />
                </div>
                <div className="input-container ic2">
                  <input className="input"
                    name='Expression'
                    id='Expression'
                    onChange={onChange}
                    placeholder="Expression"
                    required
                  />
                </div>
                <button className="submit" type='submit'>Submit</button>
              </form>
            </div>
          </div>
        </div>
      </div>
    </>
  );
}

export default Animes;