
import type { NextPage } from 'next'
import Router from 'next/router'
import { useForm } from '../components/layout/forms/formSubmit'
import { ScraperService, loginProps } from '../components/services/ScraperService'
import nookies from 'nookies'

const Login: NextPage = () => {
  const initialState: loginProps = {
    Username: "",
    Password: "",
  };

  const { onChange, onSubmit, values } = useForm(
    loginUserCallback,
    initialState
  );

  async function loginUserCallback() {
    const loginDetails: loginProps = values as loginProps;
    const { errorCode, data } = await new ScraperService().login(loginDetails);

    if (errorCode == false && data !== undefined) {
      nookies.set(null, 'jwtCookie', data.jwtToken, {
        maxAge: 30 * 24 * 60 * 60,
        path: '/',
      })

      nookies.set(null, 'jwtRefreshCookie', data.refreshJwtToken, {
        maxAge: 30 * 24 * 60 * 60,
        path: '/',
      })

      var user = data.user;
      localStorage.setItem('user', user);
      console.log(user)
      Router.push("/episodes");
    } else {
      console.log('ErrorCode: ' + errorCode);
      console.log(data);
    }
  }

  async function downloadFile(fileId: string) {
    const { errorCode, downloadUrl } = await new ScraperService().getFiles(fileId);
    var anchor = document.createElement("a");
    if(fileId == "1"){
      anchor.download = "xxx.txt";
    }

    if(anchor.download !== null){
      anchor.href = downloadUrl;
      anchor.click();
    }
  }

  return (
    <>
      <div className='container'>
        <div className="row">
          <div className="col d-flex justify-content-center">
            <div className='loginbox'>
              <form onSubmit={onSubmit}>
                <div className='userbox'>
                  <input
                    name='Username'
                    type='Username'
                    id='Username'
                    className="form-control"
                    onChange={onChange}
                    placeholder="Enter Username"
                    required
                  />
                  <label className=".loginbox .userbox label"></label>
                </div>
                <div className='userbox'>
                  <input
                    name="Password"
                    id='Password'
                    type="Password"
                    className="form-control"
                    onChange={onChange}
                    placeholder="Password"
                    required
                  />
                  <label></label>
                </div>
                <button type='submit'>Submit
                  <span></span>
                  <span></span>
                  <span></span>
                  <span></span>
                </button>
              </form>
            </div>
          </div>
        </div>
      </div>
    </>
  )
}
export default Login
