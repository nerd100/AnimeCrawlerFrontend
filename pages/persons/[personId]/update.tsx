import type { GetServerSideProps, NextPage } from "next";
import Router from "next/router";
import { requireAuthentication } from "../../../components/hoc/requireAuthentication";
import { useForm } from "../../../components/layout/forms/formSubmit";
import { ScraperService, PersonType, PersonDetailResponse, PersonDetailType } from "../../../components/services/ScraperService";

export const getServerSideProps: GetServerSideProps = requireAuthentication(async (context) => {
  const { personId } = context.query;
  const { errorCode, data } = await new ScraperService().getPerson(context, personId);

  return {
    props: { errorCode, data }
  }
})

const UpdatePerson: NextPage<PersonDetailResponse> = ({errorCode, data}) => {
  const { onChange, onSubmit, values } = useForm(
    updatePersonCallback,
    data
  );

  let persondata = values as PersonType;

  async function updatePersonCallback() {
    const personDetails: PersonType = values as PersonType;
    personDetails.Geburtstag = personDetails.Geburtstag.split('T')[0]; //openApi validator accept only date not dateTime
    const { errorCode, data } = await new ScraperService().updatePerson(personDetails, personDetails.Id.toString());
    if (errorCode == false && data !== undefined) {
      Router.push("/persons");
    } else {
      console.log('ErrorCode: ' + errorCode);
      console.log(data)
    }
  }
  
  var birthdayDate = new Date(persondata.Geburtstag)
  var formatedDay = ("0" + birthdayDate.getDate().toString()).slice(-2);
  var formatedMonth = ("0" + (birthdayDate.getMonth()+1).toString()).slice(-2);
  var formatedBirthday = `${birthdayDate.getFullYear()}-${formatedMonth}-${formatedDay}`

  return (
    <>
      <div className="container">
        <div className="row">
          <div className="col d-flex justify-content-center">
              <form className="form pull-right" onSubmit={onSubmit}>
                <h2 className="title">Update New Person</h2>
                <div className="input-container">
                  <input className="input"
                    name='Vorname'
                    id='Vorname'
                    value={persondata.Vorname}
                    onChange={onChange}
                    placeholder="Vorname"
                    required
                  />
                </div>
                <div className="input-container ic2">
                  <input className="input"
                    name='Nachname'
                    id='Nachname'
                    value={persondata.Nachname}
                    onChange={onChange}
                    placeholder="Nachname"
                    required
                  />
                </div>
                <div className="input-container ic2">
                  <input className="input"
                    name='Strasse'
                    id='Strasse'
                    value={persondata.Strasse}
                    onChange={onChange}
                    placeholder="Strasse"
                    required
                  />
                </div>
                <div className="input-container ic2">
                  <input className="input"
                    name='Hausnummer'
                    id='Hausnummer'
                    value={persondata.Hausnummer}
                    onChange={onChange}
                    placeholder="Hausnummer"
                    required
                  />
                </div>
                <div className="input-container ic2">
                  <input className="input"
                    name='Plz'
                    id='Plz'
                    value={persondata.Plz}
                    onChange={onChange}
                    placeholder="Plz"
                    required
                  />
                </div>
                <div className="input-container ic2">
                  <input className="input"
                    name='Ort'
                    id='Ort'
                    value={persondata.Ort}
                    onChange={onChange}
                    placeholder="Ort"
                    required
                  />
                </div>
                <div className="input-container ic2">
                  <input className="input"
                    name='Geburtstag'
                    id='Geburtstag'
                    type='date'
                    value={formatedBirthday}
                    onChange={onChange}
                    required
                  />
                </div>
                <button className="submit" type='submit'>Submit</button>
              </form>
            </div>
        </div>
      </div>
    </>
  );
}

export default UpdatePerson;