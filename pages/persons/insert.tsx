import type { NextPage } from "next";
import { useForm } from "../../components/layout/forms/formSubmit";
import { PersonResponse, ScraperService, PersonType } from "../../components/services/ScraperService";

const InsertPerson: NextPage<PersonResponse> = () => {
  const initialState: PersonType = {
    Id: 0,
    Vorname: "",
    Nachname: "",
    Strasse: "",
    Hausnummer: "",
    Plz: "",
    Ort: "",
    Geburtstag: null
  };

  const { onChange, onSubmit, values } = useForm(
    addPersonCallback,
    initialState
  );

  async function addPersonCallback() {
    const personDetails: PersonType = values as PersonType;
    const { errorCode, data } = await new ScraperService().addPerson(personDetails);
    if (errorCode == false && data !== undefined) {
      window.location.reload();
    } else {
      console.log('ErrorCode: ' + errorCode);
      console.log(data);
    }
  }

  return (
    <>
      <div className="container">
        <div className="row">
          <div className="col d-flex justify-content-center">
              <form className="form pull-right" onSubmit={onSubmit}>
                <h2 className="title">Add New Person</h2>
                <div className="input-container">
                  <input className="input"
                    name='Vorname'
                    id='Vorname'
                    onChange={onChange}
                    placeholder="Vorname"
                    min="0"
                    data-bind="value:replyNumber"
                    required
                  />
                </div>
                <div className="input-container ic2">
                  <input className="input"
                    name='Nachname'
                    id='Nachname'
                    onChange={onChange}
                    placeholder="Nachname"
                    required
                  />
                </div>
                <div className="input-container ic2">
                  <input className="input"
                    name='Strasse'
                    id='Strasse'
                    onChange={onChange}
                    placeholder="Strasse"
                    required
                  />
                </div>
                <div className="input-container ic2">
                  <input className="input"
                    name='Hausnummer'
                    id='Hausnummer'
                    onChange={onChange}
                    placeholder="Hausnummer"
                    required
                  />
                </div>
                <div className="input-container ic2">
                  <input className="input"
                    name='Plz'
                    id='Plz'
                    onChange={onChange}
                    placeholder="Plz"
                    required
                  />
                </div>
                <div className="input-container ic2">
                  <input className="input"
                    name='Ort'
                    id='Ort'
                    onChange={onChange}
                    placeholder="Ort"
                    required
                  />
                </div>
                <div className="input-container ic2">
                  <input className="input"
                    name='Geburtstag'
                    id='Geburtstag'
                    type='date'
                    onChange={onChange}
                    required
                  />
                </div>
                <button className="submit" type='submit'>Submit</button>
              </form>
            </div>
        </div>
      </div>
    </>
  );
}

export default InsertPerson;