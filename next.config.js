module.exports = {
  flags: {
    DEV_SSR: false,
  },
  reactStrictMode: true,
  env: {
    BACKEND_BASE_URL: process.env.BACKEND_BASE_URL_TEST,
    SECRET: process.env.SECRET,
    REFRESH_SECRET: process.env.REFRESH_SECRET,
    SPOTIFY_CLIENTID: process.env.SPOTIFY_CLIENTID,
    SPOTIFY_CLIENTSECRET: process.env.SPOTIFY_CLIENTSECRET,
    SPOTIFY_REFRESHTOKEN: process.env.SPOTIFY_REFRESHTOKEN,
  }
}
