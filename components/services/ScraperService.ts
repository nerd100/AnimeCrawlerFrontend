
import nookies from 'nookies'
import { type } from 'os';
import { ViewStatus } from '../enums/enums'
import axiosInstance from './interceptorService'

export type AnimeType = {
    Id: number;
    Name: string;
    Expression: string;
}

export type AnimeList = {
    list: AnimeType[]
}

export type AnimeResponse = {
    errorCode: boolean | number;
    data: AnimeList | null;
}

export type PersonType = {
    Id: number;
    Vorname: string;
    Nachname: string;
    Strasse: string;
    Hausnummer: string;
    Plz: string;
    Ort: string;
    Geburtstag: string;
}

export type PersonList = {
    list: PersonType[]
}

export type PersonResponse = {
    errorCode: boolean | number;
    data: PersonList | null;
}

export type PersonDetailType = {
    Id: number;
    Vorname: string;
    Nachname: string;
    Adresse: {
        Strasse: string;
        Hausnummer: string;
        Plz: string;
        Ort: string;
    }
    Geburtstag: Date;
}

export type PersonDetailResponse = {
    errorCode: boolean | number;
    data: PersonType | null;
}

export type EpisodeType = {
    Id: number,
    AnimeId: number,
    Anime: string,
    Season: number,
    Episode: number,
    StatusId: number,
    Status: string,
    Date: Date
}

export type EpisodeList = {
    list: EpisodeType[]
}

export type EpisodeResponse = {
    errorCode: boolean | number;
    data: EpisodeList | null;
}

export type EpisodeAndAnimeResponse = {
    errorCode: boolean | number;
    tables: AnimeList | null;
    elements: EpisodeList | null;
}

export type AuthResponse = {
    errorCode: boolean | number;
    data: AuthType | null;
}

export type AuthType = {
    jwtToken: string | null;
    jwtRefreshToken: string | null;
}

export type RefreshTokenResponse = {
    errorCode: number;
    data: RefreshTokenType | null;
}

export type RefreshTokenType = {
    jwtToken: string | null;
    jwtRefreshToken: string | null;
}

export type loginProps = {
    Username: string;
    Password: string;
}

export type animeProps = {
    AnimeId: number;
    Name: string;
    Expression: string;
}

export type UserObject = {
    Id: number;
}

export type UserResponse = {
    errorCode: boolean | number;
    data: authProps | null;
}

export type authProps = {
    user: any,
    jwtToken: string;
    refreshJwtToken: string;
}

export function cookieSetter(context: any, dataResponse: any) {
    try {
        const { jwtCookie } = nookies.get(context);
        if (jwtCookie !== dataResponse.config.headers.Authorization) {
            console.log("SET COOKIE")
            const jwtToken: string = dataResponse.config.headers.Authorization;
            const jwtRefreshToken: string = dataResponse.config.headers.Refresh;
            nookies.set(context, 'jwtCookie', jwtToken, {
                maxAge: 30 * 24 * 60 * 60,
                path: '/',
                sameSite: 'none'
            })
            nookies.set(context, 'jwtRefreshCookie', jwtRefreshToken, {
                maxAge: 30 * 24 * 60 * 60,
                path: '/',
                sameSite: 'none'
            })
        }
    } catch (error) {
        console.log(error)
    }
}

export class ScraperService {
    private readonly BackendBaseUrl = "http://localhost:5000";

    public async getTokenByRefreshToken(refreshToken: string): Promise<RefreshTokenResponse> {
        try {
            const dataResponse = await fetch(
                `${this.BackendBaseUrl}/refreshToken`,
                {
                    method: 'POST',
                    headers: {
                        'Accept': 'application/json',
                        'Content-Type': 'application/json',
                    },
                    body: JSON.stringify({"refreshToken":refreshToken})
                });

            const errorCode = dataResponse.status
            const data = await dataResponse.json();

            return {
                errorCode,
                data
            };
        } catch (error) {
            console.log("error");
            return {
                errorCode: error.status,
                data: null
            }
        }
    }

    public async login(loginData: loginProps): Promise<UserResponse> {
        try {
            const requestData = JSON.stringify(loginData);
            const dataResponse = await fetch(
                `${this.BackendBaseUrl}/login`,
                {
                    method: 'POST',
                    headers: {
                        'Accept': 'application/json',
                        'Content-Type': 'application/json',
                    },
                    body: requestData
                });

            const errorCode = dataResponse.ok ? false : dataResponse.status
            const data = await dataResponse.json();
            
            return {
                errorCode,
                data
            };
        } catch (error) {
            
            return {
                errorCode: 500,
                data: error
            };
        }
    }

    public async getEpisodes(context: any): Promise<EpisodeResponse> {
        try {

            const dataResponse = await axiosInstance.get(
                `${this.BackendBaseUrl}/episodes/list`,
                {
                    method: 'GET',
                    headers: {
                        Authorization: context.req.headers.Authorization,
                        Refresh: context.req.headers.Refresh
                    }
                },
            );

            const errorCode = dataResponse.status;
            const data: EpisodeList = await dataResponse.data;
            
            cookieSetter(context, dataResponse);

            return {
                errorCode,
                data
            };
        } catch (error) {
           
            return {
                errorCode: 500,
                data: null,
            }
        }
    }

    public async updateEpisodeState(id: number, state: ViewStatus): Promise<any> {
        try {
            console.log(JSON.stringify({ Id: 18, StatusId: 1 }))
            const dataResponse = await fetch(
                `${this.BackendBaseUrl}/episodes/update`,
                {
                    method: 'POST',
                    headers: {
                        'Accept': 'application/json',
                        'Content-Type': 'application/json',
                    },
                    body: JSON.stringify({ Id: id, StatusId: state })
                },
            );

            const errorCode = dataResponse.status;
            const data: any = null;

            return {
                errorCode,
                data
            };
        } catch (error) {
            return {
                errorCode: 500,
                data: null,
            }
        }
    }

    public async getAnimes(context: any): Promise<AnimeResponse> {
        try {
            const dataResponse = await axiosInstance.get(
                `${this.BackendBaseUrl}/animes/list`,
                {
                    method: 'GET',
                    headers: {
                        Authorization: context.req.headers.Authorization,
                        Refresh: context.req.headers.Refresh
                    }
                }
            );
            console.log(dataResponse)
            const errorCode = dataResponse.status;
            const data: AnimeList = await dataResponse.data;

            cookieSetter(context, dataResponse);

            return {
                errorCode,
                data
            };
        } catch (error) {
            return {
                errorCode: 500,
                data: null
            }
        }
    }

    public async addAnime(animeDetails: animeProps): Promise<AnimeResponse> {
        try {
            const dataResponse = await fetch(
                `${this.BackendBaseUrl}/animes/insert`,
                {
                    method: 'POST',
                    headers: {
                        'Accept': 'application/json',
                        'Content-Type': 'application/json',
                    },
                    body: JSON.stringify(animeDetails)
                }
            );

            const errorCode = dataResponse.ok ? false : dataResponse.status;
            const data: AnimeList = await dataResponse.json();

            return {
                errorCode,
                data
            };
        } catch (error) {
            return {
                errorCode: 500,
                data: null
            }
        }
    }

    public async deleteAnime(id: number): Promise<AnimeResponse> {
        try {
            const dataResponse = await fetch(
                `${this.BackendBaseUrl}/animes/delete?id=${id}`,
                {
                    method: 'DELETE'
                }
            );

            const errorCode = dataResponse.ok ? false : dataResponse.status;
            const data: AnimeList = await dataResponse.json();

            return {
                errorCode,
                data
            };
        } catch (error) {
            return {
                errorCode: 500,
                data: null
            }
        }
    }

    public async getPersons(context: any): Promise<PersonResponse> {
        try {
            const dataResponse = await axiosInstance.get(
                `${this.BackendBaseUrl}/person/list`,
                {
                    method: 'GET',
                    headers: {
                        Authorization: context.req.headers.Authorization,
                        Refresh: context.req.headers.Refresh
                    }
                }
            );
            const errorCode = dataResponse.status;
            const data: PersonList = await dataResponse.data;

            cookieSetter(context, dataResponse);

            return {
                errorCode,
                data
            };
        } catch (error) {
            return {
                errorCode: 500,
                data: null
            }
        }
    }

    public async addPerson(personDetails: PersonType): Promise<any> {
        try {
            const dataResponse = await fetch(
                `${this.BackendBaseUrl}/person/insert`,
                {
                    method: 'POST',
                    headers: {
                        'Accept': 'application/json',
                        'Content-Type': 'application/json',
                    },
                    body: JSON.stringify(personDetails)
                }
            );

            const errorCode = dataResponse.ok ? false : dataResponse.status;
            const data: any = await dataResponse.json();

            return {
                errorCode,
                data
            };
        } catch (error) {
            return {
                errorCode: 500,
                data: null
            }
        }
    }

    public async getPerson(context: any, personId: string): Promise<PersonDetailResponse> {
        try {
            const dataResponse = await axiosInstance.get(
                `${this.BackendBaseUrl}/person/details?id=${personId}`,
                {
                    method: 'GET',
                    headers: {
                        Authorization: context.req.headers.Authorization,
                        Refresh: context.req.headers.Refresh
                    }
                }
            );

            const errorCode = dataResponse.status;
            const data: PersonType = await dataResponse.data;

            cookieSetter(context, dataResponse);

            return {
                errorCode,
                data
            };
        } catch (error) {
            return {
                errorCode: 500,
                data: null
            }
        }
    }

    public async updatePerson(personDetails: PersonType, personId: string): Promise<any> {
        try {
            const dataResponse = await fetch(
                `${this.BackendBaseUrl}/person/update?id=${personId}`,
                {
                    method: 'POST',
                    headers: {
                        'Accept': 'application/json',
                        'Content-Type': 'application/json',
                    },
                    body: JSON.stringify(personDetails)
                }
            );

            const errorCode = dataResponse.ok ? false : dataResponse.status;
            const data: any = await dataResponse.json();

            return {
                errorCode,
                data
            };
        } catch (error) {
            return {
                errorCode: 500,
                data: null
            }
        }
    }

    public async deletePerson(personId: string): Promise<any> {
        try {
            const dataResponse = await fetch(
                `${this.BackendBaseUrl}/person/delete?id=${personId}`,
                {
                    method: 'DELETE',
                    headers: {
                        'Accept': 'application/json',
                        'Content-Type': 'application/json',
                    }
                }
            );

            const errorCode = dataResponse.ok ? false : dataResponse.status;
            const data: any = await dataResponse.json();

            return {
                errorCode,
                data
            };
        } catch (error) {
            return {
                errorCode: 500,
                data: null
            }
        }
    }


    public async getSpotify(): Promise<string> {
        try {
            const spotify_clientId = process.env.SPOTIFY_CLIENTID || '11dbf778bd68488e81bf6b4c27d0585e';
            const spotify_clientSecret = process.env.SPOTIFY_CLIENTSECRET || '6dcb7308f6ff4c92a2e4b8f4cf1806db';
            const spotify_refreshToken = process.env.SPOTIFY_REFRESHTOKEN || 'AQCZbZjoiws_qDCkuEaMJrFDqMco-LeyxlhqCWQQLw7A7AlxBzVj3T7QiCrpF0IZ6p9f8JUz8PVk43ks6tfLMHFK45qXfmg5Z1kfa1S1yYK-OW0g_TgSbiBecljHTQv5Ocg';

            const dataResponse = await axiosInstance.get(
                `${this.BackendBaseUrl}/spotify/song?clientId=${spotify_clientId}&clientSecret=${spotify_clientSecret}&refreshToken=${spotify_refreshToken}`,
                {
                    method: 'GET',
                    headers: {
                    }
                },
            );
            
            const data = dataResponse.data;
            return data;
        } catch (error) {
            return "No Data Found"
        }
    }

    public async getFiles(fileId: string): Promise<any> {
        try {

            const dataResponse = await axiosInstance.get(
                `${this.BackendBaseUrl}/files/file?fileId=${fileId}`,
                {
                    method: 'GET',
                    responseType: 'blob'
                },
            );
            const errorCode = dataResponse.status;
            const data = await dataResponse.data;
            var downloadUrl = window.URL.createObjectURL(data);
            
            return {
                errorCode,
                downloadUrl
            };
        } catch (error) {
            console.log(error);
            return {
                errorCode: 500,
                data: null,
            }
        }
    }
}