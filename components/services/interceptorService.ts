import axios from 'axios';
import { ScraperService } from './ScraperService';


const axiosInstance = axios.create(
  {
    baseURL: "http://localhost:3000",
    headers: {
      "Content-Type": "application/json",
    },
  }
);

// request interceptor for adding token
axiosInstance.interceptors.request.use(
  (req) => {
    return req;
  });


axiosInstance.interceptors.response.use(
  (res) => {
    return res;
  }, async (err) => {
    const originalConfig = err.config;

    if (originalConfig.url !== "/login" && err.response) {
      // Access Token was expired
      if (err.response.status === 401 && !originalConfig._retry) {
        originalConfig._retry = true;
        try {
          const rs = await new ScraperService().getTokenByRefreshToken(err.config.headers.Refresh);

          if (rs.errorCode === 500) {
            originalConfig.headers.Authorization = "";
            originalConfig.headers.Refresh = "";
          }
          if (rs.errorCode === 200) {
            originalConfig.headers.Authorization = rs.data.jwtToken
            originalConfig.headers.Refresh = rs.data.jwtRefreshToken
          }
          return axiosInstance(originalConfig);
        } catch (_error) {
          console.log(_error)
          return Promise.reject(_error);
        }
      }
    }
    return Promise.reject(err);
  }

);
export default axiosInstance;