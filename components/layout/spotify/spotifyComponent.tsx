import React, { ReactNode, useState } from 'react';
import { ScraperService } from '../../services/ScraperService';

 async function getSpotifySong() {
    const song = await new ScraperService().getSpotify();
    return song;
};


class Spotify extends React.Component<{}, { spotifySong: string }> {
    constructor(props) {
        super(props);
        // Set initial state (ONLY ALLOWED IN CONSTRUCTOR)
        this.state = {
            spotifySong: ''
        };
    }
    componentDidMount() {
        console.log("TimeComponent Mounted...")
        setInterval(async () => this.setState({ spotifySong:  await getSpotifySong() }), 1000);
      }

    render() {
        return(
            <div>
                <div style={{color: "white"}}>{this.state.spotifySong}</div>
            </div>
        );
    }
}

export default Spotify