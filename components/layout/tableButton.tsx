import React from "react";
import { ViewStatus } from "../enums/enums";
import { ScraperService } from "../services/ScraperService";

async function changeState(id: number, state: ViewStatus){
  await new ScraperService().updateEpisodeState(id, state);
  window.location.reload();
}

class TableButton extends React.Component<{ id: any, statusId: number, buttonStyle: string}, {}> {

    render() {
      return (
        <div>
          <button className={`btn btn-${this.props.buttonStyle}`} onClick={ async () => { await changeState(this.props.id, this.props.statusId)} }>{this.props.children}</button>
        </div>
      );
    }
  }

export default TableButton;
