import { ReactNode, useEffect, useState } from "react";
import nookies from 'nookies'
import Router from "next/router";
import Link from "next/link";
import Spotify from "../spotify/spotifyComponent";

export type HeaderLayoutProps = {
    children?: ReactNode;
}

function logout() {
    nookies.destroy(null, "jwtCookie");
    nookies.destroy(null, "jwtRefreshCookie");
    Router.push("/");
}

function ShowLogoutButton() {
    if (typeof window !== "undefined") {
        if (window.location.pathname == "/") {
            return <></>
        }
        return <button className="btn btn-success col-0" onClick={logout}>Logout</button>
    } else {
        return <></>
    }
}

function ShowNavButtons() {
    const [mounted, setMounted] = useState(false);
    useEffect(() => {
        setMounted(true)
    }, [])

    if (typeof window !== "undefined") {
        if (window.location.pathname == "/") {
            return mounted && <></>
        }
        return mounted && (
            <>
                <div className="navbar fixed-top navbar-light bg-dark">
                    <div className="nav-item nav-link">
                        <Link href="/episodes">
                            <button className="button-36" role="button">Home</button>
                            </Link>
                    </div>
                    <div className="nav-item nav-link">
                        <Link href="/episodes">
                        <button className="button-36" role="button">Anime</button>
                        </Link>
                    </div>
                    <div className="nav-item nav-link">
                        <Link href="/persons">
                        <button className="button-36" role="button">Person</button>
                        </Link>
                    </div>
                    <div className="nav-item nav-link">
                        <button className="button-36" style={{backgroundColor: 'red'}} onClick={logout}>Logout</button>
                    </div>
                </div>
            </>
        )
    } else {
        return mounted && <></>
    }
}

export default function HeaderLayout(props: HeaderLayoutProps) {
    return (
        <>
            <nav>
                <ShowNavButtons />
            </nav>
            <main>
                {props.children}
            </main>
        </>
    )
}