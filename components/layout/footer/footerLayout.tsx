import { ReactNode } from "react";
import nookies from 'nookies'
import Router from "next/router";
import Link from "next/link";
import Spotify from "../spotify/spotifyComponent";

export type FooterLayoutProps = {
    children?: ReactNode;
}

export default function FooterLayout(props: FooterLayoutProps) {
    return (
        <footer className="footer">
            <Spotify></Spotify>
        </footer>
    )
}