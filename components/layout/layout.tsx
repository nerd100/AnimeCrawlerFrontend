import { ReactNode } from "react";
import Head from 'next/head'
import HeaderLayout from "./header/headerLayout";
import FooterLayout from "./footer/footerLayout";

type Props = {
    children?: ReactNode
}

const Layout = ({ children }: Props) => (
    <div>
        <Head>
            <title>AnimeScraper</title>
            <meta name="Description" content="Hallo" />
            <meta name="viewport" content="width=device-width, initial-scale=1" />
            <meta name="theme-color" content="#f21c0a" />
        </Head>
        <HeaderLayout />
        {children}
        <FooterLayout />
    </div>
)

export default Layout