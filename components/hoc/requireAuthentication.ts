
export function requireAuthentication(gssp) {
    console.log("login failed1")
    return async (context) => {
        const { req } = context;
        const token = req.cookies.jwtCookie;
        const refreshToken = req.cookies.jwtRefreshCookie;
        console.log("login failed2")
        if (!token || !refreshToken) {
            // Redirect to login page
            console.log("login failed3")
            return {
                redirect: {
                    destination: '/',
                    statusCode: 302
                }
            };
        }
        req.headers.Authorization = token;
        req.headers.Refresh = refreshToken;

        return await gssp(context); // Continue on to call `getServerSideProps` logic
    }
}